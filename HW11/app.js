"use strict";

const iconPassword = document.querySelectorAll(".icon-password");
const textPassword = document.querySelectorAll(".password");
const submitButton = document.querySelector(".btn");

const arrayOfIconPassword = [...iconPassword];
const arrayOfTextPassword = [...textPassword];

function changeIconPassword(array) {
  array.forEach((icon, index) => {
    icon.addEventListener("click", () => {
      if (icon.classList.contains("fa-eye")) {
        icon.classList.remove("fa-eye");
        icon.classList.add("fa-eye-slash");
        arrayOfTextPassword[index].setAttribute("type", "password");
      } else {
        icon.classList.remove("fa-eye-slash");
        icon.classList.add("fa-eye");
        arrayOfTextPassword[index].setAttribute("type", "text");
      }
    });
  });
}

changeIconPassword(arrayOfIconPassword);

const message = document.createElement("div");
message.style.color = "red";

submitButton.addEventListener("click", (element) => {
  element.preventDefault();
  if (arrayOfTextPassword[0].value !== arrayOfTextPassword[1].value) {
    arrayOfTextPassword[1].parentElement.append(message);
    message.innerText = "Потрібно ввести однакові значення";
  } else if (
    arrayOfTextPassword[0].value.length > 0 &&
    arrayOfTextPassword[1].value.length > 0 &&
    arrayOfTextPassword[0].value === arrayOfTextPassword[1].value
  ) {
    message.remove();
    alert("You are welcome!");
  }
});
